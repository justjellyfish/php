<?php
require_once 'connection.php';


$link = mysqli_connect($host, $user, $password, $database);

if(mysqli_connect_errno())
{
	echo 'connection error number ', mysqli_connect_errno(), ': ', mysqli_connect_error();
	exit();
}






$sql = "CREATE TABLE IF NOT EXISTS mentor(
	name varchar(60) NOT NULL, 
	works_since date NOT NULL,
	salary int(6) NOT NULL, 
	diploma varchar(15) NOT NULL, 
	passport_data varchar(11) NOT NULL,
	date_of_birth date NOT NULL,
	PRIMARY KEY (passport_data))";

if (mysqli_query($link, $sql)) {
	echo "table mentor has been created";
} else {
	echo "error: " . mysqli_error($link);
}
echo "<br>";

$sqql = "INSERT INTO mentor VALUES (
	'Sirius Black','2000-12-08',15000,'65939243','0491994953','1980-03-08'),
	('Rubeus Hagrid','1992-07-05',20000,'15436457','0256547477','1969-01-03'),
	('Severus Snape','2000-11-23',15000,'79364880','0390245020','1981-10-10')";

if ($link->query($sqql) === TRUE) {
    echo "values have been put into mentor";
} else {
    echo "error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";






$sql = "CREATE TABLE IF NOT EXISTS medical_info(
	number varchar(11) NOT NULL, 
	blood_type varchar(5) NOT NULL,
	date_of_first_info date NOT NULL, 
	height_cm int(3) NOT NULL, 
	weight_kg int(3) NOT NULL,
	PRIMARY KEY (number))";

if (mysqli_query($link, $sql)) {
	echo "table medical_info has been created";
} else {
	echo "error: " . mysqli_error($link);
}
echo "<br>";

$sqql = "INSERT INTO medical_info VALUES(
	'125373','III+','2013-01-15',125,26), 
	('124456','IV-','2007-11-04',160,43),
	('132721','II-','2012-02-19',142,41)";

if ($link->query($sqql) === TRUE) {
    echo "values have been put into medical_info";
} else {
    echo "error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";







$sql = "CREATE TABLE IF NOT EXISTS personal_info(
	number varchar(11) NOT NULL,
	reason varchar(80) NOT NULL,
	payment int(7) NOT NULL, 
	first_parent_info varchar(80) NOT NULL,
	second_parent_info varchar(80) NOT NULL, 
	assignment_number varchar(11) NOT NULL,
	PRIMARY KEY (number))";

if (mysqli_query($link, $sql)) {
	echo "table personal_info has been created";
} else {
	echo "error: " . mysqli_error($link);
}
echo "<br>";

$sqql = "INSERT INTO personal_info VALUES('125234','parents are deprived of parental rights', 260,'no info','deprived of parental rights', 398), 
	('136267', 'the death of parents',600, 'dead','dead', 177),
	('136836','voluntary renunciation',0,'alive', 'held in custody', 179)";

if ($link->query($sqql) === TRUE) {
    echo "values have been put into personal_info";
} else {
    echo "error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";







$sql = "CREATE TABLE IF NOT EXISTS teacher ( 
    name varchar(60) NOT NULL,
    works_since date NOT NULL,
    salary int(6) NOT NULL,
    diploma varchar(15) NOT NULL,
    passport_data varchar(11) NOT NULL,
    date_of_birth date NOT NULL,
    PRIMARY KEY (passport_data))";

if (mysqli_query($link, $sql)) {
	echo "table teacher has been created";
} else {
	echo "error: ", mysqli_error($link);
}
echo "<br>";

$sqql = "INSERT INTO teacher VALUES (
	'Albus Dumbledore', '2017-10-25', 14500, '1942004', '0812389510', '1974-09-18'),
	('Sybill Trelawney', '2017-07-02', 14000, '1263462', '0815347421', '1960-11-03'),
	('Remus Lupin', '1999-03-02', 15000, '2534477', '0811361611', '1976-03-04')";

if ($link->query($sqql) === TRUE) {
    echo "values have been put into teacher";
} else {
    echo "error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";






$sql = "CREATE TABLE IF NOT EXISTS doctor(
	name varchar(60) NOT NULL, 
	started_working_at date NOT NULL,
	specialization varchar(30) NOT NULL, 
	salary int(7) NOT NULL, 
	passport_data varchar(11) NOT NULL,
	date_of_birth date NOT NULL, 
	diploma int(15) NOT NULL,
	PRIMARY KEY (name))";

if (mysqli_query($link, $sql)) {
	echo "table doctor has been created";
} else {
	echo "error: " . mysqli_error($link);
}
echo "<br>";

$sqql = "INSERT INTO doctor VALUES (
	'Godric Gryffindor','1992-08-16', 'cardiologist', 	40000, '0816789383','1959-05-23','13618897'),
	('Helga Hufflepuff','1999-04-30', 'oculist', 	25000, '0860918007','1971-12-22','26887690'),
	('Rowena Ravenclaw','1993-03-01', 'surgeon', 	45000,'0824701300','1959-11-05','27230298')";

if ($link->query($sqql) === TRUE) {
    echo "values have been put into doctor";
} else {
    echo "error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";







$sql = "CREATE TABLE IF NOT EXISTS child(
	passport_data varchar(11) NOT NULL,
	name varchar(60) NOT NULL, 
	date_of_birth date NOT NULL, 
	here_since date NOT NULL, 
	medical_info_number varchar(11) NOT NULL,
	room_number int(3) NOT NULL, 
	personal_info_number varchar(11) NOT NULL, 
	place_of_birth varchar(60) NOT NULL,
	mentor_passport_data varchar(60) NOT NULL,
	PRIMARY KEY (passport_data),
	FOREIGN KEY (medical_info_number) REFERENCES medical_info(number),
	FOREIGN KEY (mentor_passport_data) REFERENCES mentor(passport_data),
	FOREIGN KEY (personal_info_number) REFERENCES personal_info(number))";

if (mysqli_query($link, $sql)) {
	echo "table child has been created";
} else {
	echo "error: " . mysqli_error($link);
}
echo "<br>";

$sqql = "INSERT INTO child VALUES (
	'0813324354', 'Harry', '2011-10-03',  	'2014-01-18', '124456', 140, '125234', 'London', '0491994953'),
	('0823515626', 'Ronald', '2005-05-03', 	'2007-11-04', '125373', 120, '136267', 	'Boston','0256547477'),
	('0825648931', 'Hermione', '2009-11-05', 	'2015-12-06', '132721', 240, '136836', 'Paris', '0390245020')";

if ($link->query($sqql) === TRUE) {
    echo "values have been put into child";
} else {
    echo "error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";


$sql = "CREATE TABLE IF NOT EXISTS classes(
	number int(6) NOT NULL,
	date date NOT NULL, 
	starts_at time NOT NULL,
	ends_at time NOT NULL, 
	classroom_number smallint(3) NOT NULL, 
	subject varchar(30) NOT NULL,
	teacher_passport_data varchar(60) NOT NULL, 
	child_passport_data varchar(11) NOT NULL,
	PRIMARY KEY (number),
	FOREIGN KEY (teacher_passport_data) REFERENCES teacher(passport_data),
	FOREIGN KEY (child_passport_data) REFERENCES child(passport_data))";


if (mysqli_query($link, $sql)) {
	echo "table classes has been created";
} else {
	echo "error: ", mysqli_error($link);
}
echo "<br>";

$sqql = "INSERT INTO classes VALUES (
	1095, '2018-02-26', '13:30:00', '14:30:00', 288, 	'astronomy', '0812389510', '0823515626'), 
	(1241, '2018-02-01', '12:40:00', '13:40:00', 303, 'literature', '0815347421', '0825648931'),
	(1255, '2018-02-12', '12:00:00', '13:00:00', 112, 	'computer science', '0811361611', '0813324354')";

if ($link->query($sqql) === TRUE) {
    echo "values have been put into classes";
} else {
    echo "error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";







$sql = "CREATE TABLE IF NOT EXISTS consultation(
	number int(10) NOT NULL, 
	date date NOT NULL,
	starts_at time NOT NULL, 
	ends_at time NOT NULL,
	child_passport_data varchar(11) NOT NULL,
	doctor_name varchar(60) NOT NULL,
	PRIMARY KEY (number),
	FOREIGN KEY (child_passport_data) REFERENCES child(passport_data),
	FOREIGN KEY (doctor_name) REFERENCES doctor(name))";

if (mysqli_query($link, $sql)) {
	echo "table consultation has been created";
} else {
	echo "error: " . mysqli_error($link);
}
echo "<br>";

$sqql = "INSERT INTO consultation VALUES (
	122, '2018-02-26', '13:00:00', '13:50:00', '0823515626', 	'Godric Gryffindor'), 
	(125, '2018-02-15', '09:30:00', '11:20:00', '0825648931', 	'Rowena Ravenclaw'),
	(312, '2018-02-12', '10:40:00', '12:30:00', '0813324354', 	'Helga Hufflepuff')";

if ($link->query($sqql) === TRUE) {
    echo "values have been put into consultation";
} else {
    echo "error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";







$sql = "CREATE TABLE IF NOT EXISTS vaccination(
	number int(11) NOT NULL, 
	date date NOT NULL, 
	test_result varchar(40) NOT NULL,
	against varchar(50) NOT NULL, 
	next_date date NOT NULL, 
	medical_info_number varchar(11) NOT NULL,
	PRIMARY KEY (number),
	FOREIGN KEY (medical_info_number) REFERENCES medical_info(number))";

if (mysqli_query($link, $sql)) {
	echo "table vaccination has been created";
} else {
	echo "error: " . mysqli_error($link);
}
echo "<br>";

$sqql = "INSERT INTO vaccination VALUES
	(101, '2018-02-13', 'reactive', 'dipylidium infection', '2018-02-23', '125373'),
	(104, '2017-01-01', 'non-reactive', 'diphyllobothrium infection', '2017-04-01', '124456'),
	(132, '2018-01-31', 'non-reactive', 'diphtheria', '2018-02-27','132721')";

if ($link->query($sqql) === TRUE) {
    echo "values have been put into vaccination";
} else {
    echo "error: " . $sqqll . "<br>" . $link->error;
}
echo "<br>";



?>